package edu.sjsu.android.dataincome.models;

import android.os.Parcel;
import android.os.Parcelable;

public class ExpenseData implements Parcelable {
    private final int dataId;
    private final float amount;
    private String type;
    private final int userId;
    private final String date;
    private final String note;

    public ExpenseData(int dataId, float amount, String type, int userId, String note, String date) {
        this.dataId = dataId;
        this.amount = amount;
        this.type = type;
        this.userId = userId;
        this.date = date;
        this.note = note;
    }

    protected ExpenseData(Parcel in) {
        dataId = in.readInt();
        amount = in.readFloat();
        type = in.readString();
        userId = in.readInt();
        date = in.readString();
        note = in.readString();
    }

    public static final Creator<ExpenseData> CREATOR = new Creator<ExpenseData>() {
        @Override
        public ExpenseData createFromParcel(Parcel in) {
            return new ExpenseData(in);
        }

        @Override
        public ExpenseData[] newArray(int size) {
            return new ExpenseData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(dataId);
        dest.writeFloat(amount);
        dest.writeString(type);
        dest.writeInt(userId);
        dest.writeString(date);
        dest.writeString(note);
    }

    public int getDataId() {
        return dataId;
    }

    public float getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getUserId() {
        return userId;
    }

    public String getDate() {
        return date;
    }

    public String getNote() {
        return note;
    }
}
