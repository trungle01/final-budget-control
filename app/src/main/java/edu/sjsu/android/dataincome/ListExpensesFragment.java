package edu.sjsu.android.dataincome;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.os.StrictMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import edu.sjsu.android.dataincome.adapters.ExpensesAdapter;
import edu.sjsu.android.dataincome.database.BudgetDB;
import edu.sjsu.android.dataincome.models.ExpenseData;
import edu.sjsu.android.dataincome.models.User;

public class ListExpensesFragment extends Fragment implements ExpensesAdapter.OnExpenseListListener{
    private BudgetDB database;
    private User currentUser;
    public static final String USER_KEY = "current_user";
    private ArrayList<ExpenseData> expenses;
    private ExpensesAdapter expensesAdapter;

    private final String AUTHORITY_EXPENSE = "edu.sjsu.android.dataincome.providers.ExpenseDataProvider";
    private final Uri CONTENT_URI_EXPENSE = Uri.parse("content://" + AUTHORITY_EXPENSE);

    // For auto send email
    private static final String USER_NAME = "budgetcontrolapp@gmail.com";  // GMail user name (just the part before "@gmail.com")
    private static final String PASSWORD = "ControlBudget!1"; // GMail password

    // support language
    private final String deviceLocale = Locale.getDefault().getCountry();

    public ListExpensesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        database = new BudgetDB(getContext());
        // get currentUser from Login
        Bundle bundle = getArguments();
        if(bundle != null){
            currentUser = bundle.getParcelable(USER_KEY);
            expenses = database.getAllExpenses_ByUserId(currentUser.getUserId());
        }
        View view = inflater.inflate(R.layout.fragment_list_expenses, container, false);
        expensesAdapter = new ExpensesAdapter(expenses, this);
        RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setAdapter(expensesAdapter);

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                String msg, ansyes, ansno;
                if(deviceLocale.equals("VN")){
                    msg = "Bạn có chắc muốn xóa mục này?";
                    ansyes = "CÓ";
                    ansno = "KHÔNG";
                }else{
                    msg = "Are you sure to delete this record?";
                    ansyes = "YES";
                    ansno = "NO";
                }
                new AlertDialog.Builder(viewHolder.itemView.getContext()).setMessage(msg).setPositiveButton(ansyes, (dialog, which) -> {
                    int position = viewHolder.getLayoutPosition();
                    // handle in database
                    // Get current income based on position
                    ExpenseData currentExpense= expenses.get(position);
                    int i_userid = currentExpense.getUserId();
                    // add new income to database
                    String where = "userId = " + i_userid + " AND _id = " + currentExpense.getDataId();
                    if (requireActivity().getContentResolver().delete(CONTENT_URI_EXPENSE, where, null) > 0){
                        expenses.remove(position);
                        expensesAdapter.notifyItemRemoved(position);
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Xóa chi tiêu thành công!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Delete Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                        }
                        // handle send email if total income < total expense
                        float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                        float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                        if(totalExpense > totalIncome){
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                            }
                            // call send email
                            sendEmail();
                        }
                    }else{
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Xóa chi tiêu thất bại!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Delete Expense Failed!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    // call refresh view
                    refreshView();
                }).setNegativeButton(ansno, (dialog, which) -> {
                    dialog.dismiss();
                    // call refresh view
                    refreshView();
                }).create().show();
            }
        };

        ItemTouchHelper mItemTouch = new ItemTouchHelper(simpleCallback);
        mItemTouch.attachToRecyclerView(recyclerView);

        // StrictMode for auto send email
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        return view;
    }

    @Override
    public void onListClick(int position, List<ExpenseData> list) {
        AlertDialog.Builder mydialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        View myView = inflater.inflate(R.layout.custom_layout_for_update_datas, null);
        mydialog.setView(myView);
        final AlertDialog dialog = mydialog.create();
        dialog.setCancelable(false);

        EditText edtAmount = myView.findViewById(R.id.amount_edt);
        EditText edtType = myView.findViewById(R.id.type_edt);
        EditText edtNote = myView.findViewById(R.id.note_edt);

        Button btnUpdate = myView.findViewById(R.id.btnUpdate);
        Button btnCancel = myView.findViewById(R.id.btnCancel);
        Button btnDelete = myView.findViewById(R.id.btnDelete);

        // Get current income based on position
        ExpenseData currentExpense = expenses.get(position);
        // set value for amount, type, and note of current income
        edtAmount.setText(String.valueOf(currentExpense.getAmount()));
        edtType.setText(currentExpense.getType());
        edtNote.setText(currentExpense.getNote());

        btnUpdate.setOnClickListener(v -> {
            String amount = edtAmount.getText().toString().trim();
            String type = edtType.getText().toString().trim();
            String note = edtNote.getText().toString().trim();

            if(TextUtils.isEmpty(amount)){
                if(deviceLocale.equals("VN")){
                    edtAmount.setError("mục số tiền là cần thiết!!!");
                }else{
                    edtAmount.setError("amount is required!!!");
                }
                return;
            }
            if(TextUtils.isEmpty(type)){
                if(deviceLocale.equals("VN")){
                    edtType.setError("mục loại là cần thiết!!!");
                }else{
                    edtType.setError("type is required!!!");
                }
                return;
            }
            if(TextUtils.isEmpty(note)){
                if(deviceLocale.equals("VN")){
                    edtNote.setError("mục chú thích là cần thiết!!!");
                }else{
                    edtNote.setError("note is required!!!");
                }
                return;
            }

            // check if float has exactly 2 decimal numbers after "."
            String[] split;
            if(amount.contains(".")) {
                split = amount.split("\\.");
                if (split[1].length() > 2) {
                    if (deviceLocale.equals("VN")) {
                        edtAmount.setError("Tối đa 2 số sau dấu '.'!!!");
                    } else {
                        edtAmount.setError("Maximum 2 decimal numbers after '.' !!!");
                    }
                }else{
                    float i_amount = Float.parseFloat(amount);
                    // case amount, type and note are not changed => not update => call dialog.dismiss()
                    if(currentExpense.getAmount() == i_amount && currentExpense.getType().equalsIgnoreCase(type) && currentExpense.getNote().equalsIgnoreCase(note)){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Không có mục nào được cập nhật!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Nothing is updated!!!", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        int i_userid = currentExpense.getUserId();
                        String date = currentExpense.getDate();

                        ContentValues contentValues = new ContentValues();
                        // data values to contentValues
                        contentValues.put("userid", i_userid);
                        contentValues.put("amount", i_amount);
                        contentValues.put("type", type);
                        contentValues.put("note", note);
                        contentValues.put("date", date);
                        // add new income to database
                        String where = "userId = " + i_userid + " AND _id = " + currentExpense.getDataId();
                        if (requireActivity().getContentResolver().update(CONTENT_URI_EXPENSE, contentValues, where, null) > 0){
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Cập nhật thành công!!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Update Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                            }
                            // handle send email if total income < total expense
                            float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                            float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                            if(totalExpense > totalIncome){
                                if(deviceLocale.equals("VN")){
                                    Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                                }
                                // call send email
                                sendEmail();
                            }
                        }else{
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Cập nhật thành công!!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Update Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    dialog.dismiss();
                    // call refresh view
                    refreshView();
                }
            }else{
                float i_amount = Float.parseFloat(amount);
                // case amount, type and note are not changed => not update => call dialog.dismiss()
                if(currentExpense.getAmount() == i_amount && currentExpense.getType().equalsIgnoreCase(type) && currentExpense.getNote().equalsIgnoreCase(note)){
                    if(deviceLocale.equals("VN")){
                        Toast.makeText(getActivity(), "Không có mục nào được cập nhật!!!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Nothing is updated!!!", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    int i_userid = currentExpense.getUserId();
                    String date = currentExpense.getDate();

                    ContentValues contentValues = new ContentValues();
                    // data values to contentValues
                    contentValues.put("userid", i_userid);
                    contentValues.put("amount", i_amount);
                    contentValues.put("type", type);
                    contentValues.put("note", note);
                    contentValues.put("date", date);
                    // add new income to database
                    String where = "userId = " + i_userid + " AND _id = " + currentExpense.getDataId();
                    if (requireActivity().getContentResolver().update(CONTENT_URI_EXPENSE, contentValues, where, null) > 0){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Cập nhật thành công!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Update Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                        }
                        // handle send email if total income < total expense
                        float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                        float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                        if(totalExpense > totalIncome){
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                            }
                            // call send email
                            sendEmail();
                        }
                    }else{
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Cập nhật thành công!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Update Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                dialog.dismiss();
                // call refresh view
                refreshView();
            }
        });

        btnDelete.setOnClickListener( v ->  {
            String msg, ansyes, ansno;
            if(deviceLocale.equals("VN")){
                msg = "Bạn có chắc muốn xóa mục này?";
                ansyes = "CÓ";
                ansno = "KHÔNG";
            }else{
                msg = "Are you sure to delete this record?";
                ansyes = "YES";
                ansno = "NO";
            }
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Alert!!");
            alert.setMessage(msg);
            alert.setPositiveButton(ansyes, (dialog1, which) -> {
                int i_userid = currentExpense.getUserId();
                // add new income to database
                String where = "userId = " + i_userid + " AND _id = " + currentExpense.getDataId();
                if (requireActivity().getContentResolver().delete(CONTENT_URI_EXPENSE, where, null) > 0){
                    if(deviceLocale.equals("VN")){
                        Toast.makeText(getActivity(), "Xóa chi tiêu thành công!!!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Delete Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                    }
                    // handle send email if total income < total expense
                    float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                    float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                    if(totalExpense > totalIncome){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                        }
                        // call send email
                        sendEmail();
                    }
                }else{
                    if(deviceLocale.equals("VN")){
                        Toast.makeText(getActivity(), "Xóa chi tiêu thất bại!!!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Delete Expense Failed!!!", Toast.LENGTH_SHORT).show();
                    }
                }
                dialog1.dismiss();
                // call refresh view
                refreshView();
            });
            alert.setNegativeButton(ansno, (dialog12, which) -> dialog12.dismiss());
            alert.show();
            dialog.dismiss();
        });

        btnCancel.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);         // replace main_frame with fragment
        fragmentTransaction.commit();
    }

    public void refreshView(){
        ListExpensesFragment listExpensesFragment = new ListExpensesFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(USER_KEY, currentUser);
        listExpensesFragment.setArguments(bundle);
        setFragment(listExpensesFragment);
    }

    private void sendEmail(){
        final String RECIPIENT = currentUser.getEmail();
        final String subject = "Warning from BudgetControl!";
        final String messageToSend = "Over Budget. Please control your expense!!!";
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port","587");
        Session session = Session.getInstance(props, new javax.mail.Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USER_NAME, PASSWORD);
            }
        });
        try {
            Message message = new MimeMessage(session);
            // set sender for email
            message.setFrom(new InternetAddress(USER_NAME));
            // set recipient for email
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(RECIPIENT));
            // set email subject
            message.setSubject(subject);
            // set email content
            message.setText(messageToSend);
            Transport.send(message);
            if(deviceLocale.equals("VN")){
                Toast.makeText(getActivity(), "Email nhắc nhở đã được gởi thành công!!!", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Reminder email is sent successfully!", Toast.LENGTH_LONG).show();
            }
        }catch (MessagingException ex){
            throw new RuntimeException(ex);
        }
    }
}