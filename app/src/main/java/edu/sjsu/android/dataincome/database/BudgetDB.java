package edu.sjsu.android.dataincome.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

import edu.sjsu.android.dataincome.models.ExpenseData;
import edu.sjsu.android.dataincome.models.IncomeData;
import edu.sjsu.android.dataincome.models.User;

public class BudgetDB extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "BudgetControl.db";
    private static final int VERSION = 1;

    // Variables for users table
    private static final String TABLE_USERS = "users";
    private static final String USER_COLUMN_ID = "_id";
    private static final String USER_COLUMN_USERNAME = "username";
    private static final String USER_COLUMN_PASSWORD = "password";
    private static final String USER_COLUMN_EMAIL = "email";
    private static final String USER_COLUMN_PHONE = "phone";

    // Variables for income data table
    private static final String TABLE_INCOMES = "incomes";
    private static final String INCOME_COLUMN_ID = "_id";
    private static final String INCOME_COLUMN_USER_ID = "userId";
    private static final String INCOME_COLUMN_AMOUNT = "amount";
    private static final String INCOME_COLUMN_TYPE = "type";
    private static final String INCOME_COLUMN_NOTE = "note";
    private static final String INCOME_COLUMN_DATE = "date";

    // Variables for expense data table
    private static final String TABLE_EXPENSES= "expenses";
    private static final String EXPENSE_COLUMN_ID = "_id";
    private static final String EXPENSE_COLUMN_USER_ID = "userId";
    private static final String EXPENSE_COLUMN_AMOUNT = "amount";
    private static final String EXPENSE_COLUMN_TYPE = "type";
    private static final String EXPENSE_COLUMN_NOTE = "note";
    private static final String EXPENSE_COLUMN_DATE = "date";

    private static final String create_table_user_query = "CREATE TABLE " + TABLE_USERS
            + "("
            + USER_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + USER_COLUMN_USERNAME + " TEXT NOT NULL,"
            + USER_COLUMN_PASSWORD + " TEXT NOT NULL,"
            + USER_COLUMN_EMAIL + " TEXT NOT NULL,"
            + USER_COLUMN_PHONE + " TEXT NOT NULL"
            + ")";

    private static final String create_table_income_query = "CREATE TABLE " + TABLE_INCOMES
            + "("
            + INCOME_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + INCOME_COLUMN_USER_ID + " INTEGER NOT NULL,"
            + INCOME_COLUMN_AMOUNT + " INTEGER NOT NULL,"
            + INCOME_COLUMN_TYPE + " TEXT NOT NULL,"
            + INCOME_COLUMN_NOTE + " TEXT NOT NULL,"
            + INCOME_COLUMN_DATE + " TEXT NOT NULL"
            + ")";

    private static final String create_table_expense_query = "CREATE TABLE " + TABLE_EXPENSES
            + "("
            + EXPENSE_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + EXPENSE_COLUMN_USER_ID + " INTEGER NOT NULL,"
            + EXPENSE_COLUMN_AMOUNT + " INTEGER NOT NULL,"
            + EXPENSE_COLUMN_TYPE + " TEXT NOT NULL,"
            + EXPENSE_COLUMN_NOTE + " TEXT NOT NULL,"
            + EXPENSE_COLUMN_DATE + " TEXT NOT NULL"
            + ")";

    public BudgetDB(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // ========== start: handle table users ==================
        // drop table users if exists
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        // create table users
        sqLiteDatabase.execSQL(create_table_user_query) ;
        // ========== end: handle table users ==================v

        // ========== start: handle table incomes ==================
        // drop table incomes if exists
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_INCOMES);
        // create table incomes
        sqLiteDatabase.execSQL(create_table_income_query) ;
        // ========== end: handle table expenses ==================

        // ========== start: handle table expenses ==================
        // drop table expenses if exists
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSES);
        // create table expenses
        sqLiteDatabase.execSQL(create_table_expense_query) ;
        // ========== end: handle table expenses ==================
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_INCOMES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSES);
    }

    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(create_table_user_query);
        sqLiteDatabase.execSQL(create_table_income_query);
        sqLiteDatabase.execSQL(create_table_expense_query);
        sqLiteDatabase.setVersion(1);
    }

    //============== Start: Methods for Users Table ===============================//
    // Helper methods of User Table
    // Insert new user to table users in database
    public long insertUser(ContentValues contentValues){
        SQLiteDatabase database = getWritableDatabase();
        // contentValues is got from from register
        return database.insert(TABLE_USERS, null, contentValues);
    }

    // Helper methods of User Table
    // Check if user is exist when Register
    public Boolean checkUsername(String username){
        SQLiteDatabase database = getWritableDatabase();
        @SuppressLint("Recycle") Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_USERS + " WHERE username = ?", new String[]{username});
        return cursor.getCount() > 0;
    }

    // Helper methods of User Table
    // Check if username or password is not correct when Login
    public Boolean checkUsernamePassword(String username, String password){
        SQLiteDatabase database = getWritableDatabase();
        @SuppressLint("Recycle") Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_USERS + " WHERE username = ? AND password = ?", new String[]{username, password});
        return cursor.getCount() > 0;
    }

    // Helper methods of User Table
    // Get all user information for sending to Homepage
    public User getCurrentUser(String username){
        SQLiteDatabase database = getWritableDatabase();
        // run after check user exists, therefore user is always exist => cursor.getCount() > 0
        @SuppressLint("Recycle") Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_USERS + " WHERE username = ?", new String[]{username});
        if (cursor != null) {
            cursor.moveToFirst();
        }
        assert cursor != null;
        String userId = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
        String password = cursor.getString(cursor.getColumnIndexOrThrow("password"));
        String email = cursor.getString(cursor.getColumnIndexOrThrow("email"));
        String phone = cursor.getString(cursor.getColumnIndexOrThrow("phone"));
        return new User(Integer.parseInt(userId), username, password, email, phone);
    }

    public Cursor getAllUsers(String orderBy){
        SQLiteDatabase database = getWritableDatabase();
        return database.query(TABLE_USERS, new String[]{USER_COLUMN_ID, USER_COLUMN_USERNAME, USER_COLUMN_PASSWORD, USER_COLUMN_EMAIL, USER_COLUMN_PHONE}, null, null,null,null, orderBy);
    }

    //============== End: Methods for Users Table ===============================//


    //============== Start: Methods for Incomes Table ===============================//
    // Helper methods of incomes Table
    // Insert new data to table incomes in database
    public long insertIncome(ContentValues contentValues){
        SQLiteDatabase database = getWritableDatabase();
        // contentValues is got from from register
        return database.insert(TABLE_INCOMES, null, contentValues);
    }

    public ArrayList<IncomeData> getLastIncomeData_ByUserId(int userid){
        SQLiteDatabase database = getWritableDatabase();
        ArrayList<IncomeData> incomes = new ArrayList<>();
        // run after check user exists, therefore user is always exist => cursor.getCount() > 0
        String strQuery = "SELECT * FROM " + TABLE_INCOMES + " WHERE " + INCOME_COLUMN_USER_ID + " = " + userid + " ORDER BY " + INCOME_COLUMN_DATE + " DESC LIMIT 1";
        @SuppressLint("Recycle") Cursor res = database.rawQuery(strQuery, null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            String i_id = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_ID));
            String i_userId = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_USER_ID));
            String i_amount = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_AMOUNT));
            String i_type = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_TYPE));
            String i_note = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_NOTE));
            String i_date= res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_DATE));
            // create new income object
            IncomeData income = new IncomeData(Integer.parseInt(i_id), Float.parseFloat(i_amount), i_type, Integer.parseInt(i_userId), i_note, i_date);
            incomes.add(income);
            res.moveToNext();
        }
        return incomes;
    }

    public Cursor getAllIncomes(String orderBy){
        SQLiteDatabase database = getWritableDatabase();
        return database.query(TABLE_INCOMES, new String[]{INCOME_COLUMN_ID, INCOME_COLUMN_USER_ID, INCOME_COLUMN_AMOUNT, INCOME_COLUMN_TYPE, INCOME_COLUMN_NOTE, INCOME_COLUMN_DATE}, null, null,null,null, orderBy);
    }

    public ArrayList<IncomeData> getAllIncomes_ByUserId(int userId){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<IncomeData> incomes = new ArrayList<>();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("SELECT * FROM incomes WHERE userId='" + userId + "' ORDER BY " + INCOME_COLUMN_DATE + " DESC", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            String i_id = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_ID));
            String i_userId = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_USER_ID));
            String i_amount = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_AMOUNT));
            String i_type = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_TYPE));
            String i_note = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_NOTE));
            String i_date= res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_DATE));
            // create new income object
            IncomeData income = new IncomeData(Integer.parseInt(i_id), Float.parseFloat(i_amount), i_type, Integer.parseInt(i_userId), i_note, i_date);
            incomes.add(income);
            res.moveToNext();
        }
        return incomes;
    }

    public float getTotalIncome_ByUserId(int userId){
        float total = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("SELECT * FROM incomes WHERE userId='" + userId + "'", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            String i_amount = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_AMOUNT));
            total += Float.parseFloat(i_amount);
            res.moveToNext();
        }
        return total;
    }

    public int updateIncome(String tableName, ContentValues contentValues, String where){
        SQLiteDatabase database = getWritableDatabase();
        return database.update(tableName, contentValues, where, null);
    }

    public int deleteIncome(String tableName, String where){
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(tableName, where, null);
    }

    //============== End: Methods for Users Table ===============================//



    //============== Start: Methods for Expenses Table ===============================//
    // Helper methods of Expenses Table
    // Insert new data to table Expenses in database
    public long insertExpense(ContentValues contentValues){
        SQLiteDatabase database = getWritableDatabase();
        // contentValues is got from from register
        return database.insert(TABLE_EXPENSES, null, contentValues);
    }

    public ArrayList<ExpenseData> getLastExpenseData_ByUserId(int userid){
        SQLiteDatabase database = getWritableDatabase();
        ArrayList<ExpenseData> expenses = new ArrayList<>();
        // run after check user exists, therefore user is always exist => cursor.getCount() > 0
        String strQuery = "SELECT * FROM " + TABLE_EXPENSES + " WHERE " + EXPENSE_COLUMN_USER_ID + " = " + userid + " ORDER BY " + EXPENSE_COLUMN_DATE + " DESC LIMIT 1";
        @SuppressLint("Recycle") Cursor res = database.rawQuery(strQuery, null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            String i_id = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_ID));
            String i_userId = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_USER_ID));
            String i_amount = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_AMOUNT));
            String i_type = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_TYPE));
            String i_note = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_NOTE));
            String i_date= res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_DATE));
            // create new income object
            ExpenseData expense = new ExpenseData(Integer.parseInt(i_id), Float.parseFloat(i_amount), i_type, Integer.parseInt(i_userId), i_note, i_date);
            expenses.add(expense);
            res.moveToNext();
        }
        return expenses;
    }

    public Cursor getAllExpenses(String orderBy){
        SQLiteDatabase database = getWritableDatabase();
        return database.query(TABLE_EXPENSES, new String[]{EXPENSE_COLUMN_ID, EXPENSE_COLUMN_USER_ID, EXPENSE_COLUMN_AMOUNT, EXPENSE_COLUMN_TYPE, EXPENSE_COLUMN_NOTE, EXPENSE_COLUMN_DATE}, null, null,null,null, orderBy);
    }

    public ArrayList<ExpenseData> getAllExpenses_ByUserId(int userId){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<ExpenseData> expenses = new ArrayList<>();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("SELECT * FROM expenses WHERE userId='" + userId + "' ORDER BY " + EXPENSE_COLUMN_DATE + " DESC", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            String i_id = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_ID));
            String i_userId = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_USER_ID));
            String i_amount = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_AMOUNT));
            String i_type = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_TYPE));
            String i_note = res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_NOTE));
            String i_date= res.getString(res.getColumnIndexOrThrow(EXPENSE_COLUMN_DATE));
            // create new income object
            ExpenseData expense = new ExpenseData(Integer.parseInt(i_id), Float.parseFloat(i_amount), i_type, Integer.parseInt(i_userId), i_note, i_date);
            expenses.add(expense);
            res.moveToNext();
        }
        return expenses;
    }

    // need to finish this
    public float getTotalExpense_ByUserId(int userId){
        float total = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("SELECT * FROM expenses WHERE userId='" + userId + "'", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            String i_amount = res.getString(res.getColumnIndexOrThrow(INCOME_COLUMN_AMOUNT));
            total += Float.parseFloat(i_amount);
            res.moveToNext();
        }
        return total;
    }

    public int updateExpense(String tableName, ContentValues contentValues, String where){
        SQLiteDatabase database = getWritableDatabase();
        return database.update(tableName, contentValues, where, null);
    }

    public int deleteExpense(String tableName, String where){
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(tableName, where, null);
    }

    //============== End: Methods for Users Table ===============================//


}
