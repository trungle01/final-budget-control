package edu.sjsu.android.dataincome.models;

import android.os.Parcel;
import android.os.Parcelable;

public class IncomeData implements Parcelable {
    private final int dataId;
    private final float amount;
    private String type;
    private final int userId;
    private final String date;
    private final String note;

    public IncomeData(int id, float amount, String type, int userId, String note, String date) {
        this.amount = amount;
        this.type = type;
        this.dataId = id;
        this.userId = userId;
        this.note = note;
        this.date = date;
    }

    protected IncomeData(Parcel in) {
        dataId = in.readInt();
        userId = in.readInt();
        amount = in.readFloat();
        type = in.readString();
        note = in.readString();
        date = in.readString();
    }

    public static final Creator<IncomeData> CREATOR = new Creator<IncomeData>() {
        @Override
        public IncomeData createFromParcel(Parcel in) {
            return new IncomeData(in);
        }

        @Override
        public IncomeData[] newArray(int size) {
            return new IncomeData[size];
        }
    };

    public float getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDataId() {
        return dataId;
    }

    public int getUserId() {
        return userId;
    }

    public String getNote() {
        return note;
    }

    public String getDate() {
        return date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(dataId);
        dest.writeInt(userId);
        dest.writeFloat(amount);
        dest.writeString(type);
        dest.writeString(note);
        dest.writeString(date);
    }
}