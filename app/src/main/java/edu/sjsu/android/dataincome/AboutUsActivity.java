package edu.sjsu.android.dataincome;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import edu.sjsu.android.dataincome.databinding.ActivityAboutUsBinding;
import edu.sjsu.android.dataincome.models.User;

public class AboutUsActivity extends AppCompatActivity {
    public static final String USER_KEY = "current_user";
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        edu.sjsu.android.dataincome.databinding.ActivityAboutUsBinding binding = ActivityAboutUsBinding.inflate(getLayoutInflater());
        // get currentUser from Login
        if(getIntent().getExtras() != null){
            currentUser = getIntent().getParcelableExtra(USER_KEY);
        }
        binding.btnCall.setOnClickListener(this::dialPhone);
        binding.btnBackToHomePage.setOnClickListener(this::backToHome);
        setContentView(binding.getRoot());
    }

    private void backToHome(View view) {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.putExtra(USER_KEY, currentUser);
        startActivity(intent);
    }

    private void dialPhone(View view) {
        // figure out how to dial call
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+14086794123")));
    }
}