package edu.sjsu.android.dataincome.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

import edu.sjsu.android.dataincome.databinding.IncomeRecyclerDataBinding;
import edu.sjsu.android.dataincome.models.IncomeData;

public class IncomesAdapter extends RecyclerView.Adapter<IncomesAdapter.IncomeViewHolder> {
    private final List<IncomeData> mIncomesValues;
    private final OnIncomeListListener monIncomeListListener;

    public IncomesAdapter(List<IncomeData> mValues, OnIncomeListListener monIncomeListListener) {
        this.mIncomesValues = mValues;
        this.monIncomeListListener = monIncomeListListener;
    }

    @NonNull
    @Override
    public IncomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new IncomeViewHolder(IncomeRecyclerDataBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false), monIncomeListListener);
    }

    @Override
    public void onBindViewHolder(@NonNull IncomesAdapter.IncomeViewHolder holder, int position) {
        // group is a list. get value from mValues
        IncomeData income = mIncomesValues.get(position);
        holder.binding.dateTxtIncome.setText(income.getDate());
        holder.binding.noteTxtIncome.setText(income.getNote());
        holder.binding.typeTxtIncome.setText(income.getType());
        holder.binding.amountTxtIncome.setText(String.valueOf(income.getAmount()));
    }

    @Override
    public int getItemCount() {
        return mIncomesValues.size();
    }

    public class IncomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final IncomeRecyclerDataBinding binding;
        OnIncomeListListener onIncomeListListener;

        public IncomeViewHolder(@NonNull IncomeRecyclerDataBinding binding, OnIncomeListListener m_onIncomeListListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onIncomeListListener = m_onIncomeListListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onIncomeListListener.onListClick(getAdapterPosition(), mIncomesValues);
        }
    }

    public interface OnIncomeListListener {
        void onListClick(int position, List<IncomeData> list);
    }
}


