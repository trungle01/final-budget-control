package edu.sjsu.android.dataincome;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import edu.sjsu.android.dataincome.databinding.ActivityProfileBinding;
import edu.sjsu.android.dataincome.models.User;

public class ProfileActivity extends AppCompatActivity {
    public static final String USER_KEY = "current_user";
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        edu.sjsu.android.dataincome.databinding.ActivityProfileBinding binding = ActivityProfileBinding.inflate(getLayoutInflater());
        binding.btnGoToHomePage.setOnClickListener(this::gotoHomePage);
        // get currentUser from Login
        if(getIntent().getExtras() != null){
            currentUser = getIntent().getParcelableExtra(USER_KEY);
        }
        // display current user
        binding.username.setText(currentUser.getUsername());
        binding.password.setText(currentUser.getPassword());
        binding.email.setText(currentUser.getEmail());
        binding.phone.setText(currentUser.getPhone());
        binding.username.setEnabled(false);
        binding.password.setEnabled(false);
        binding.email.setEnabled(false);
        binding.phone.setEnabled(false);
        setContentView(binding.getRoot());
    }

    // Work on this in the next version
    private void gotoHomePage(View view) {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.putExtra(USER_KEY, currentUser);
        startActivity(intent);
    }
}