package edu.sjsu.android.dataincome;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.StrictMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import edu.sjsu.android.dataincome.adapters.ExpensesAdapter;
import edu.sjsu.android.dataincome.adapters.IncomesAdapter;
import edu.sjsu.android.dataincome.database.BudgetDB;
import edu.sjsu.android.dataincome.databinding.FragmentDashboardBinding;
import edu.sjsu.android.dataincome.models.ExpenseData;
import edu.sjsu.android.dataincome.models.IncomeData;
import edu.sjsu.android.dataincome.models.User;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.Locale;
import java.util.Properties;

public class DashboardFragment extends Fragment {
    private FragmentDashboardBinding binding;
    private BudgetDB database;
    private boolean isOpen = false;
    private User currentUser;
    public static final String USER_KEY = "current_user";

    private final String AUTHORITY_INCOME = "edu.sjsu.android.dataincome.providers.IncomeDataProvider";
    private final Uri CONTENT_URI_INCOME = Uri.parse("content://" + AUTHORITY_INCOME);

    private final String AUTHORITY_EXPENSE = "edu.sjsu.android.dataincome.providers.ExpenseDataProvider";
    private final Uri CONTENT_URI_EXPENSE = Uri.parse("content://" + AUTHORITY_EXPENSE);

    // Animation.
    private Animation FadeOpen, FadeClose;

    // For auto send email
    private static final String USER_NAME = "budgetcontrolapp@gmail.com";  // GMail user name (just the part before "@gmail.com")
    private static final String PASSWORD = "ControlBudget!1"; // GMail password

    // support language
    private final String deviceLocale = Locale.getDefault().getCountry();
    DecimalFormat df = new DecimalFormat("#.##");

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDashboardBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        database = new BudgetDB(getContext());
        // Animation
        FadeOpen = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_open);
        FadeClose = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_close);
        binding.fbMainPlusBtn.setOnClickListener(this::dashboardClicked);
        binding.expenseFtBtn.setOnClickListener(this::expenseBtnClicked);
        binding.incomeFtBtn.setOnClickListener(this::incomeBtnClicked);
        // get currentUser from Login
        Bundle bundle = getArguments();
        if(bundle != null){
            currentUser = bundle.getParcelable(USER_KEY);
        }
        // refresh total incomes and total expenses
        onSetChangeTotalIncomeExpense();

        // incomes list
        ArrayList<IncomeData> incomeList = database.getLastIncomeData_ByUserId(currentUser.getUserId());
        ArrayList<ExpenseData> expenseList = database.getLastExpenseData_ByUserId(currentUser.getUserId());

        // recycler income
        LinearLayoutManager layoutManagerIncome = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerIncome.setStackFromEnd(true);
        layoutManagerIncome.setReverseLayout(true);
        binding.recyclerIncome.setHasFixedSize(true);
        binding.recyclerIncome.setClickable(false);
        binding.recyclerIncome.setLayoutManager(layoutManagerIncome);

        // recycle expense
        LinearLayoutManager layoutManagerExpense= new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManagerExpense.setStackFromEnd(true);
        layoutManagerExpense.setReverseLayout(true);
        binding.recyclerExpense.setHasFixedSize(true);
        binding.recyclerExpense.setClickable(false);
        binding.recyclerExpense.setLayoutManager(layoutManagerExpense);

        IncomesAdapter.OnIncomeListListener mOnIncomeListListener = (position, list) -> {
            AlertDialog.Builder mydialog = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater1 = LayoutInflater.from(getActivity());

            View myView = inflater1.inflate(R.layout.custom_layout_for_update_datas, null);
            mydialog.setView(myView);
            final AlertDialog dialog = mydialog.create();
            dialog.setCancelable(false);

            EditText edtAmount = myView.findViewById(R.id.amount_edt);
            EditText edtType = myView.findViewById(R.id.type_edt);
            EditText edtNote = myView.findViewById(R.id.note_edt);

            Button btnUpdate = myView.findViewById(R.id.btnUpdate);
            Button btnCancel = myView.findViewById(R.id.btnCancel);
            Button btnDelete = myView.findViewById(R.id.btnDelete);

            // Get current income based on position
            IncomeData currentIncome = incomeList.get(position);
            // set value for amount, type, and note of current income
            edtAmount.setText(String.valueOf(currentIncome.getAmount()));
            edtType.setText(currentIncome.getType());
            edtNote.setText(currentIncome.getNote());

            btnUpdate.setOnClickListener(v -> {
                String amount = edtAmount.getText().toString().trim();
                String type = edtType.getText().toString().trim();
                String note = edtNote.getText().toString().trim();

                if(TextUtils.isEmpty(amount)){
                    if(deviceLocale.equals("VN")){
                        edtAmount.setError("mục số tiền là cần thiết!!!");
                    }else{
                        edtAmount.setError("amount is required!!!");
                    }
                    return;
                }
                if(TextUtils.isEmpty(type)){
                    if(deviceLocale.equals("VN")){
                        edtType.setError("mục loại là cần thiết!!!");
                    }else{
                        edtType.setError("type is required!!!");
                    }
                    return;
                }
                if(TextUtils.isEmpty(note)){
                    if(deviceLocale.equals("VN")){
                        edtNote.setError("mục chú thích là cần thiết!!!");
                    }else{
                        edtNote.setError("note is required!!!");
                    }
                    return;
                }

                // check if float has exactly 2 decimal numbers after "."
                String[] split = amount.split("\\.");
                if(split[1].length() > 2){
                    if(deviceLocale.equals("VN")){
                        edtAmount.setError("Tối đa 2 số sau dấu '.'!!!");
                    }else{
                        edtAmount.setError("Maximum 2 decimal numbers after '.' !!!");
                    }
                }else{
                    float i_amount = Float.parseFloat(amount);
                    // case amount, type and note are not changed => not update => call dialog.dismiss()
                    if(currentIncome.getAmount() == i_amount && currentIncome.getType().equalsIgnoreCase(type) && currentIncome.getNote().equalsIgnoreCase(note)){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Không có mục nào được cập nhật!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Nothing is updated!!!", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        int i_userid = currentIncome.getUserId();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
                        String date = sdf.format(new Date());

                        ContentValues contentValues = new ContentValues();
                        // data values to contentValues
                        contentValues.put("userid", i_userid);
                        contentValues.put("amount", i_amount);
                        contentValues.put("type", type);
                        contentValues.put("note", note);
                        contentValues.put("date", date);
                        // add new income to database
                        String where = "userId = " + i_userid + " AND _id = " + currentIncome.getDataId();
                        if (requireActivity().getContentResolver().update(CONTENT_URI_INCOME, contentValues, where, null) > 0){
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Cập nhật thành công!!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Update Income Successfully!!!", Toast.LENGTH_SHORT).show();
                            }
                            // handle send email if total income < total expense
                            float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                            float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                            if(totalExpense > totalIncome){
                                if(deviceLocale.equals("VN")){
                                    Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                                }
                                // send email to user via user email address
                                sendEmail();
                            }
                        }else{
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Cập nhật thu nhập thất bại!!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Update Income Failed!!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    dialog.dismiss();
                    // call refreshView
                    refreshView();
                }
            });

            btnDelete.setOnClickListener( v ->  {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Alert!!");
                if(deviceLocale.equals("VN")){
                    alert.setMessage("Bạn có chắc muốn xóa mục này");
                }else{
                    alert.setMessage("Are you sure to delete this record");
                }
                alert.setPositiveButton("YES", (dialog1, which) -> {
                    int i_userid = currentIncome.getUserId();
                    // add new income to database
                    String where = "userId = " + i_userid + " AND _id = " + currentIncome.getDataId();
                    if (requireActivity().getContentResolver().delete(CONTENT_URI_INCOME, where, null) > 0){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Xóa thu nhập thành công!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Delete Income Successfully!!!", Toast.LENGTH_SHORT).show();
                        }
                        // handle send email if total income < total expense
                        float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                        float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                        if(totalExpense > totalIncome){
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                            }
                            // send email to user via user email address
                            sendEmail();
                        }
                    }else{
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Xóa thu nhập thất bại!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Delete Income Failed!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    dialog1.dismiss();
                    // call refresh view
                    refreshView();
                });
                alert.setNegativeButton("NO", (dialog12, which) -> dialog12.dismiss());
                alert.show();
                dialog.dismiss();
            });

            btnCancel.setOnClickListener(v -> dialog.dismiss());

            dialog.show();
        };

        ExpensesAdapter.OnExpenseListListener mOnExpenseListListener = (position, list) -> {
            AlertDialog.Builder mydialog = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater12 = LayoutInflater.from(getActivity());

            View myView = inflater12.inflate(R.layout.custom_layout_for_update_datas, null);
            mydialog.setView(myView);
            final AlertDialog dialog = mydialog.create();
            dialog.setCancelable(false);

            EditText edtAmount = myView.findViewById(R.id.amount_edt);
            EditText edtType = myView.findViewById(R.id.type_edt);
            EditText edtNote = myView.findViewById(R.id.note_edt);

            Button btnUpdate = myView.findViewById(R.id.btnUpdate);
            Button btnCancel = myView.findViewById(R.id.btnCancel);
            Button btnDelete = myView.findViewById(R.id.btnDelete);

            // Get current income based on position
            ExpenseData currentExpense = expenseList.get(position);
            // set value for amount, type, and note of current income
            edtAmount.setText(String.valueOf(currentExpense.getAmount()));
            edtType.setText(currentExpense.getType());
            edtNote.setText(currentExpense.getNote());

            btnUpdate.setOnClickListener(v -> {
                String amount = edtAmount.getText().toString().trim();
                String type = edtType.getText().toString().trim();
                String note = edtNote.getText().toString().trim();

                if(TextUtils.isEmpty(amount)){
                    if(deviceLocale.equals("VN")){
                        edtAmount.setError("mục số tiền là cần thiết!!!");
                    }else{
                        edtAmount.setError("amount is required!!!");
                    }
                    return;
                }
                if(TextUtils.isEmpty(type)){
                    if(deviceLocale.equals("VN")){
                        edtType.setError("mục loại là cần thiết!!!");
                    }else{
                        edtType.setError("type is required!!!");
                    }
                    return;
                }
                if(TextUtils.isEmpty(note)){
                    if(deviceLocale.equals("VN")){
                        edtNote.setError("mục chú thích là cần thiết!!!");
                    }else{
                        edtNote.setError("note is required!!!");
                    }
                    return;
                }

                // check if float has exactly 2 decimal numbers after "."
                String[] split = amount.split("\\.");
                if(split[1].length() > 2){
                    if(deviceLocale.equals("VN")){
                        edtAmount.setError("Tối đa 2 số sau dấu '.'!!!");
                    }else{
                        edtAmount.setError("Maximum 2 decimal numbers after '.' !!!");
                    }
                }else{
                    float i_amount = Float.parseFloat(amount);
                    // case amount, type and note are not changed => not update => call dialog.dismiss()
                    if(currentExpense.getAmount() == i_amount && currentExpense.getType().equalsIgnoreCase(type) && currentExpense.getNote().equalsIgnoreCase(note)){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Không có mục nào được cập nhật!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Nothing is updated!!!", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        int i_userid = currentExpense.getUserId();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
                        String date = sdf.format(new Date());

                        ContentValues contentValues = new ContentValues();
                        // data values to contentValues
                        contentValues.put("userid", i_userid);
                        contentValues.put("amount", i_amount);
                        contentValues.put("type", type);
                        contentValues.put("note", note);
                        contentValues.put("date", date);
                        // add new income to database
                        String where = "userId = " + i_userid + " AND _id = " + currentExpense.getDataId();

                        if (requireActivity().getContentResolver().update(CONTENT_URI_EXPENSE, contentValues, where, null) > 0){
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Cập nhật thành công!!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Update Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                            }
                            // handle send email if total income < total expense
                            float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                            float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                            if(totalExpense > totalIncome){
                                if(deviceLocale.equals("VN")){
                                    Toast.makeText(getActivity(), "Vượt ngân sách!!!", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                                }
                                // send email to user via user email address
                                sendEmail();
                            }
                        }else{
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Cập nhật thất bại!!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Update Expense Failed!!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    dialog.dismiss();
                    // call refresh view
                    refreshView();
                }
            });

            btnDelete.setOnClickListener( v ->  {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Alert!!");
                if(deviceLocale.equals("VN")){
                    alert.setMessage("Bạn có chắc muốn xóa mục này");
                }else{
                    alert.setMessage("Are you sure to delete this record");
                }
                alert.setPositiveButton("YES", (dialog13, which) -> {
                    int i_userid = currentExpense.getUserId();
                    // add new income to database
                    String where = "userId = " + i_userid + " AND _id = " + currentExpense.getDataId();
                    if (requireActivity().getContentResolver().delete(CONTENT_URI_EXPENSE, where, null) > 0){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Xóa chi tiêu thành công!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Delete Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                        }
                        // handle send email if total income < total expense
                        float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                        float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                        if(totalExpense > totalIncome){
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Vượt ngân sách!!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                            }
                            // send email to user via user email address
                            sendEmail();
                        }
                    }else{
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Xóa chi tiêu thất bại!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Delete Expense Failed!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    dialog13.dismiss();
                    // call refresh view
                    refreshView();
                });
                alert.setNegativeButton("NO", (dialog14, which) -> dialog14.dismiss());
                alert.show();
                dialog.dismiss();
            });

            btnCancel.setOnClickListener(v -> dialog.dismiss());

            dialog.show();
        };

        binding.recyclerIncome.setAdapter(new IncomesAdapter(incomeList, mOnIncomeListListener));
        binding.recyclerExpense.setAdapter(new ExpensesAdapter(expenseList, mOnExpenseListListener));
        // StrictMode for auto send email
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        // Inflate the layout for this fragment
        return view;
    }

    private void onSetChangeTotalIncomeExpense(){
        // get total incomes and total expenses of currentUser from database
        float totalIncomes = database.getTotalIncome_ByUserId(currentUser.getUserId());
        float totalExpenses = database.getTotalExpense_ByUserId(currentUser.getUserId());
        binding.incomeSetResult.setText(df.format(totalIncomes));
        binding.expenseSetResult.setText(df.format(totalExpenses));
    }

    private void dashboardClicked(View view) {
        if(isOpen){
            binding.incomeFtBtn.startAnimation(FadeClose);
            binding.expenseFtBtn.startAnimation(FadeClose);
            binding.incomeFtBtn.setClickable(false);
            binding.expenseFtBtn.setClickable(false);

            binding.incomeFtText.startAnimation(FadeClose);
            binding.expenseFtText.startAnimation(FadeClose);
            binding.incomeFtText.setClickable(false);
            binding.expenseFtText.setClickable(false);

            isOpen = false;
        }else{
            binding.incomeFtBtn.startAnimation(FadeOpen);
            binding.expenseFtBtn.startAnimation(FadeOpen);
            binding.incomeFtBtn.setClickable(true);
            binding.expenseFtBtn.setClickable(true);

            binding.incomeFtText.startAnimation(FadeOpen);
            binding.expenseFtText.startAnimation(FadeOpen);
            binding.incomeFtText.setClickable(true);
            binding.expenseFtText.setClickable(true);

            isOpen = true;
        }
    }

    private void incomeBtnClicked(View view) {
        AlertDialog.Builder mydialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        View myView = inflater.inflate(R.layout.custom_layout_for_insert_datas, null);
        mydialog.setView(myView);
        final AlertDialog dialog = mydialog.create();
        dialog.setCancelable(false);

        EditText edtAmount = myView.findViewById(R.id.amount_edt);
        EditText edtType = myView.findViewById(R.id.type_edt);
        EditText edtNote = myView.findViewById(R.id.note_edt);

        Button btnSave = myView.findViewById(R.id.btnSave);
        Button btnCancel = myView.findViewById(R.id.btnCancel);

        btnSave.setOnClickListener(v -> {
            String amount = edtAmount.getText().toString().trim();
            String type = edtType.getText().toString().trim();
            String note = edtNote.getText().toString().trim();

            if(TextUtils.isEmpty(amount)){
                if(deviceLocale.equals("VN")){
                    edtAmount.setError("mục số tiền là cần thiết!!!");
                }else{
                    edtAmount.setError("amount is required!!!");
                }
                return;
            }
            if(TextUtils.isEmpty(type)){
                if(deviceLocale.equals("VN")){
                    edtType.setError("mục loại là cần thiết!!!");
                }else{
                    edtType.setError("type is required!!!");
                }
                return;
            }
            if(TextUtils.isEmpty(note)){
                if(deviceLocale.equals("VN")){
                    edtNote.setError("mục chú thích là cần thiết!!!");
                }else{
                    edtNote.setError("note is required!!!");
                }
                return;
            }

            // check if float has exactly 2 decimal numbers after "."
            String[] split;
            if(amount.contains(".")){
                split = amount.split("\\.");
                if(split[1].length() > 2){
                    if(deviceLocale.equals("VN")){
                        edtAmount.setError("Tối đa 2 số sau dấu '.'!!!");
                    }else{
                        edtAmount.setError("Maximum 2 decimal numbers after '.' !!!");
                    }
                }else{
                    // valid
                    float i_amount = Float.parseFloat(amount);
                    int i_userid = currentUser.getUserId();
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
                    String date = sdf.format(new Date());

                    ContentValues contentValues = new ContentValues();
                    // data values to contentValues
                    contentValues.put("userid", i_userid);
                    contentValues.put("amount", i_amount);
                    contentValues.put("type", type);
                    contentValues.put("note", note);
                    contentValues.put("date", date);
                    // add new income to database
                    if (requireActivity().getContentResolver().insert(CONTENT_URI_INCOME, contentValues) != null){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Thêm thu nhập thành công!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Add Income Successfully!!!", Toast.LENGTH_SHORT).show();
                        }
                        onSetChangeTotalIncomeExpense();
                        // handle send email if total income < total expense
                        float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                        float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                        if(totalExpense > totalIncome){
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                            }
                            // send email to user via user email address
                            sendEmail();
                        }
                    }else{
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Thêm thu nhập thất bại!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Add Income Failed!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    dialog.dismiss();
                    // call refresh view
                    refreshView();
                }
            }
            else{
                // valid
                float i_amount = Float.parseFloat(amount);
                int i_userid = currentUser.getUserId();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
                String date = sdf.format(new Date());

                ContentValues contentValues = new ContentValues();
                // data values to contentValues
                contentValues.put("userid", i_userid);
                contentValues.put("amount", i_amount);
                contentValues.put("type", type);
                contentValues.put("note", note);
                contentValues.put("date", date);
                // add new income to database
                if (requireActivity().getContentResolver().insert(CONTENT_URI_INCOME, contentValues) != null){
                    if(deviceLocale.equals("VN")){
                        Toast.makeText(getActivity(), "Thêm thu nhập thành công!!!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Add Income Successfully!!!", Toast.LENGTH_SHORT).show();
                    }
                    onSetChangeTotalIncomeExpense();
                    // handle send email if total income < total expense
                    float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                    float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                    if(totalExpense > totalIncome){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                        }
                        // send email to user via user email address
                        sendEmail();
                    }
                }else{
                    if(deviceLocale.equals("VN")){
                        Toast.makeText(getActivity(), "Thêm thu nhập thất bại!!!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Add Income Failed!!!", Toast.LENGTH_SHORT).show();
                    }
                }
                dialog.dismiss();
                // call refresh view
                refreshView();
            }
        });

        btnCancel.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }

    private void expenseBtnClicked(View view) {
        AlertDialog.Builder mydialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        View myView = inflater.inflate(R.layout.custom_layout_for_insert_datas, null);
        mydialog.setView(myView);
        final AlertDialog dialog = mydialog.create();
        dialog.setCancelable(false);

        EditText edtAmount = myView.findViewById(R.id.amount_edt);
        EditText edtType = myView.findViewById(R.id.type_edt);
        EditText edtNote = myView.findViewById(R.id.note_edt);

        Button btnSave = myView.findViewById(R.id.btnSave);
        Button btnCancel = myView.findViewById(R.id.btnCancel);

        btnSave.setOnClickListener(v -> {
            String amount = edtAmount.getText().toString().trim();
            String type = edtType.getText().toString().trim();
            String note = edtNote.getText().toString().trim();

            if(TextUtils.isEmpty(amount)){
                if(deviceLocale.equals("VN")){
                    edtAmount.setError("mục số tiền là cần thiết!!!");
                }else{
                    edtAmount.setError("amount is required!!!");
                }
                return;
            }
            if(TextUtils.isEmpty(type)){
                if(deviceLocale.equals("VN")){
                    edtType.setError("mục loại là cần thiết!!!");
                }else{
                    edtType.setError("type is required!!!");
                }
                return;
            }
            if(TextUtils.isEmpty(note)){
                if(deviceLocale.equals("VN")){
                    edtNote.setError("mục chú thích là cần thiết!!!");
                }else{
                    edtNote.setError("note is required!!!");
                }
                return;
            }

            // check if float has exactly 2 decimal numbers after "."
            String[] split;
            if(amount.contains(".")){
                split = amount.split("\\.");
                if(split[1].length() > 2){
                    if(deviceLocale.equals("VN")){
                        edtAmount.setError("Tối đa 2 số sau dấu '.'!!!");
                    }else{
                        edtAmount.setError("Maximum 2 decimal numbers after '.' !!!");
                    }
                }else{
                    float i_amount = Float.parseFloat(amount);
                    int i_userid = currentUser.getUserId();
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
                    String date = sdf.format(new Date());

                    ContentValues contentValues = new ContentValues();
                    // data values to contentValues
                    contentValues.put("userId", i_userid);
                    contentValues.put("amount", i_amount);
                    contentValues.put("type", type);
                    contentValues.put("note", note);
                    contentValues.put("date", date);
                    // add new expense to database
                    if (requireActivity().getContentResolver().insert(CONTENT_URI_EXPENSE, contentValues) != null){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Thêm chi tiêu thành công!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Add Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                        }
                        onSetChangeTotalIncomeExpense();
                        // handle send email if total income < total expense
                        float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                        float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                        if(totalExpense > totalIncome){
                            if(deviceLocale.equals("VN")){
                                Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                            }
                            // send email to user via user email address
                            sendEmail();
                        }
                    }else{
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Thêm chi tiêu thất bại!!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Add Expense Failed!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    dialog.dismiss();
                    // call refresh view
                    refreshView();
                }
            }else {
                float i_amount = Float.parseFloat(amount);
                int i_userid = currentUser.getUserId();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
                String date = sdf.format(new Date());

                ContentValues contentValues = new ContentValues();
                // data values to contentValues
                contentValues.put("userId", i_userid);
                contentValues.put("amount", i_amount);
                contentValues.put("type", type);
                contentValues.put("note", note);
                contentValues.put("date", date);
                // add new expense to database
                if (requireActivity().getContentResolver().insert(CONTENT_URI_EXPENSE, contentValues) != null){
                    if(deviceLocale.equals("VN")){
                        Toast.makeText(getActivity(), "Thêm chi tiêu thành công!!!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Add Expense Successfully!!!", Toast.LENGTH_SHORT).show();
                    }
                    onSetChangeTotalIncomeExpense();
                    // handle send email if total income < total expense
                    float totalIncome = database.getTotalIncome_ByUserId(currentUser.getUserId());
                    float totalExpense = database.getTotalExpense_ByUserId(currentUser.getUserId());
                    if(totalExpense > totalIncome){
                        if(deviceLocale.equals("VN")){
                            Toast.makeText(getActivity(), "Vượt ngân sách!!", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), "Over Budget!!!", Toast.LENGTH_SHORT).show();
                        }
                        // send email to user via user email address
                        sendEmail();
                    }
                }else{
                    if(deviceLocale.equals("VN")){
                        Toast.makeText(getActivity(), "Thêm chi tiêu thất bại!!!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Add Expense Failed!!!", Toast.LENGTH_SHORT).show();
                    }
                }
                dialog.dismiss();
                // call refresh view
                refreshView();
            }
        });

        btnCancel.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);         // replace main_frame with fragment
        fragmentTransaction.commit();
    }

    public void refreshView(){
        DashboardFragment dashboardFragment = new DashboardFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(USER_KEY, currentUser);
        dashboardFragment.setArguments(bundle);
        setFragment(dashboardFragment);
    }

    private void sendEmail(){
        final String RECIPIENT = currentUser.getEmail();
        final String subject = "Warning from BudgetControl!";
        final String messageToSend = "Over Budget. Please control your expense!!!";
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port","587");
        Session session = Session.getInstance(props, new javax.mail.Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USER_NAME, PASSWORD);
            }
        });
        try {
            Message message = new MimeMessage(session);
            // set sender for email
            message.setFrom(new InternetAddress(USER_NAME));
            // set recipient for email
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(RECIPIENT));
            // set email subject
            message.setSubject(subject);
            // set email content
            message.setText(messageToSend);
            Transport.send(message);
            if(deviceLocale.equals("VN")){
                Toast.makeText(getActivity(), "Email nhắc nhở đã được gởi thành công!!!", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Reminder email is sent successfully!", Toast.LENGTH_LONG).show();
            }
        }catch (MessagingException ex){
            throw new RuntimeException(ex);
        }
    }
}