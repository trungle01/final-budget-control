package edu.sjsu.android.dataincome.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import edu.sjsu.android.dataincome.databinding.ExpenseRecyclerDataBinding;
import edu.sjsu.android.dataincome.models.ExpenseData;

public class ExpensesAdapter extends RecyclerView.Adapter<ExpensesAdapter.ExpenseViewHolder> {
    private final List<ExpenseData> mExpensesValues;
    private final OnExpenseListListener monExpenseListListener;

    public ExpensesAdapter(List<ExpenseData> mExpensesValues, OnExpenseListListener monExpenseListListener) {
        this.mExpensesValues = mExpensesValues;
        this.monExpenseListListener = monExpenseListListener;
    }

    @NonNull
    @Override
    public ExpenseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ExpenseViewHolder(ExpenseRecyclerDataBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false), monExpenseListListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpensesAdapter.ExpenseViewHolder holder, int position) {
        // group is a list. get value from mValues
        ExpenseData expense = mExpensesValues.get(position);
        holder.binding.dateTxtExpense.setText(expense.getDate());
        holder.binding.noteTxtExpense.setText(expense.getNote());
        holder.binding.typeTxtExpense.setText(expense.getType());
        holder.binding.amountTxtExpense.setText(String.valueOf(expense.getAmount()));
    }

    @Override
    public int getItemCount() {
        return mExpensesValues.size();
    }

    public class ExpenseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ExpenseRecyclerDataBinding binding;
        OnExpenseListListener onExpenseListListener;

        public ExpenseViewHolder(@NonNull ExpenseRecyclerDataBinding binding, OnExpenseListListener monIncomeListListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onExpenseListListener = monIncomeListListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onExpenseListListener.onListClick(getAdapterPosition(), mExpensesValues);
        }
    }

    public interface OnExpenseListListener{
        void onListClick(int position, List<ExpenseData> list);
    }
}
