package edu.sjsu.android.dataincome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import edu.sjsu.android.dataincome.databinding.ActivityHomeBinding;
import edu.sjsu.android.dataincome.models.User;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ActivityHomeBinding binding;
    private BottomNavigationView bottomNavigationView;
    public static final String USER_KEY = "current_user";
    private User currentUser;
    private Bundle bundle;

    // Fragment
    private DashboardFragment dashboardFragment;
    private ListIncomesFragment listIncomesFragment;
    private ListExpensesFragment listExpensesFragment;

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        // set up toolbar and title
        Toolbar toolbar = findViewById(R.id.myToolbar);
        toolbar.setTitle("Budget Manager");
        setSupportActionBar(toolbar);
        // set up buttom navigation menu
        bottomNavigationView = findViewById(R.id.bottomNavigationBar);
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        // initialize navigationview
        NavigationView navigationView = findViewById(R.id.nav);
        navigationView.setNavigationItemSelectedListener(this);
        // initialize fragments
        dashboardFragment = new DashboardFragment();
        listIncomesFragment = new ListIncomesFragment();
        listExpensesFragment = new ListExpensesFragment();
        // get currentUser from Login
        if(getIntent().getExtras() != null){
            currentUser = (User) getIntent().getParcelableExtra(USER_KEY);
        }
        // pass to dashboardFragment
        bundle = new Bundle();
        bundle.putParcelable(USER_KEY, currentUser);
        dashboardFragment.setArguments(bundle);
        // set dashboard fragment to be opened when HomePage is opened
        setFragment(dashboardFragment);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.dashboard:
                    dashboardFragment.setArguments(bundle);
                    setFragment(dashboardFragment);
                    bottomNavigationView.setItemBackgroundResource(R.color.dashboard_color);
                    break;
                case R.id.income:
                    listIncomesFragment.setArguments(bundle);
                    setFragment(listIncomesFragment);
                    bottomNavigationView.setItemBackgroundResource(R.color.income_color);
                    break;
                case R.id.expense:
                    listExpensesFragment.setArguments(bundle);
                    setFragment(listExpensesFragment);
                    bottomNavigationView.setItemBackgroundResource(R.color.expense_color);
                    break;
                default:
                    break;
            }
            return true;
        });

    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);         // replace main_frame with fragment
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        if(drawerLayout.isDrawerOpen(GravityCompat.END)){
            drawerLayout.closeDrawer(GravityCompat.END);
        }else{
            super.onBackPressed();
        }
    }

    @SuppressLint("NonConstantResourceId")
    public void displaySelectedListener(int itemId){
        Fragment fragment = null;
        switch (itemId){
            case R.id.dashboard:
                fragment = new DashboardFragment();
                fragment.setArguments(bundle);
                break;
            case R.id.income:
                fragment = new ListIncomesFragment();
                fragment.setArguments(bundle);
                break;
            case R.id.expense:
                fragment = new ListExpensesFragment();
                fragment.setArguments(bundle);
                break;
        }

        if(fragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.main_frame, fragment);
            ft.commit();
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        displaySelectedListener(item.getItemId());
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_right_menu, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.about:
                Intent about = new Intent(getApplicationContext(), AboutUsActivity.class);
                about.putExtra(USER_KEY, currentUser);
                startActivity(about);
                return true;
            case R.id.uninstall:
                Intent uninstall = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
                startActivity(uninstall);
                return true;
            case R.id.profile:
                Intent profile = new Intent(getApplicationContext(), ProfileActivity.class);
                profile.putExtra(USER_KEY, currentUser);
                startActivity(profile);
                return true;
            case R.id.logout:
                Intent logout = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(logout);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}