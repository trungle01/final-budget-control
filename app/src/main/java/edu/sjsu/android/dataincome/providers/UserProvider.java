package edu.sjsu.android.dataincome.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import edu.sjsu.android.dataincome.database.BudgetDB;

public class UserProvider extends ContentProvider {
    private BudgetDB database;
    private static final String TABLE_USERS = "users";

    public UserProvider() {
    }

    @Override
    public boolean onCreate() {
        this.database = new BudgetDB(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        // if not specified, sort by ID
        sortOrder = sortOrder == null ? "_id" : sortOrder;
        return database.getAllUsers(sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long rowID = database.insertUser(values);
        // if record is added successfully
        if(rowID > 0){
            Uri _uri = ContentUris.withAppendedId(uri, rowID);
            getContext().getContentResolver().notifyChange(_uri,null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count;
        count = database.updateIncome(TABLE_USERS, values, selection);
        return count;
    }
}

