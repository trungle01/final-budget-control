package edu.sjsu.android.dataincome;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import edu.sjsu.android.dataincome.database.BudgetDB;
import edu.sjsu.android.dataincome.databinding.ActivityRegisterBinding;

public class RegisterActivity extends AppCompatActivity {
    private ActivityRegisterBinding binding;

    private BudgetDB db;
    private ProgressDialog mDialog;

    private final String AUTHORITY = "edu.sjsu.android.dataincome.providers.UserProvider";
    private final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        binding.btnRegister.setOnClickListener(this::register);
        binding.goToLogin.setOnClickListener(this::goToLogin);
        mDialog = new ProgressDialog(this);
        db = new BudgetDB(this);
        setContentView(binding.getRoot());
    }

    private void goToLogin(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @SuppressLint("SetTextI18n")
    private void register(View view) {
        try{
            ContentValues contentValues = new ContentValues();
            // get value from register form
            String username = binding.username.getText().toString();
            String password = binding.password.getText().toString();
            String rePassword = binding.confirmPassword.getText().toString();
            String email = binding.email.getText().toString();
            String phone = binding.phone.getText().toString();
            // Validate input
            if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password) || TextUtils.isEmpty(rePassword) || TextUtils.isEmpty(email) || TextUtils.isEmpty(phone)){
                binding.warning.setError("All fields are required!!!");
                binding.warning.setText("All fields are required!!!");
                return;
            }
            // check pass and confirm pass
            if(!password.equals(rePassword)){
                binding.warning.setError("Password and Confirm Password are mismatch");
                binding.warning.setText("Password and Confirm Password are mismatch");
                return;
            }
            // display processing
            mDialog.setMessage("Processing...");
            // validate and insert new user to database
            Boolean checkUserExist = db.checkUsername(username);
            if(!checkUserExist){
                // add user when username is not exist
                // data values to contentValues
                contentValues.put("username", username);
                contentValues.put("password", password);
                contentValues.put("email", email);
                contentValues.put("phone", phone);
                // add new user to database
                if (getContentResolver().insert(CONTENT_URI, contentValues) != null){
                    Toast.makeText(this, "Register Successfully!!!", Toast.LENGTH_SHORT).show();
                    // dismiss the dialog after inserting new user failed or successfully
                    mDialog.dismiss();
                    // back to Login
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }else{
                    binding.warning.setError("Register Failed!!!");
                    binding.warning.setText("Register Failed!!!");
                    return;
                }
            }else{
                binding.warning.setError("User exists!!!");
                binding.warning.setText("User exists!!!");
                return;
            }
            // dismiss the dialog after inserting new user failed or successfully
            mDialog.dismiss();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}