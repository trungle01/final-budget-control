package edu.sjsu.android.dataincome;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import edu.sjsu.android.dataincome.database.BudgetDB;
import edu.sjsu.android.dataincome.databinding.ActivityMainBinding;
import edu.sjsu.android.dataincome.models.User;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private BudgetDB db;
    private ProgressDialog mDialog;
    public static final String USER_KEY = "current_user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        binding.btnLogin.setOnClickListener(this::login);
        binding.goToRegister.setOnClickListener(this::goToRegister);
        mDialog = new ProgressDialog(this);
        db = new BudgetDB(this);
        setContentView(binding.getRoot());
    }

    private void goToRegister(View view) {
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
    }

    @SuppressLint("SetTextI18n")
    private void login(View view) {
        String username = binding.username.getText().toString();
        String password = binding.password.getText().toString();

        if (TextUtils.isEmpty(username)){
            binding.username.setError("username is required!!!");
            binding.warning.setText("username is required!!!");
            return;
        }
        if (TextUtils.isEmpty(password)){
            binding.password.setError("password is required!!!");
            binding.warning.setText("password is required!!!");
            return;
        }
        mDialog.setMessage("Processing...");
        // check username and password from database
        Boolean checkLogin = db.checkUsernamePassword(username, password);
        mDialog.dismiss();
        if(checkLogin){
            // dismiss the dialog after inserting new user failed or successfully
            // get current user
            User currentUser = db.getCurrentUser(username);
            // After done with validation, go to Homepage
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.putExtra(USER_KEY, currentUser);
            startActivity(intent);
        }else{
            // dismiss the dialog after inserting new user failed or successfully
            binding.warning.setError("Login Failed!!!");
            binding.warning.setText("Login Failed!!!");
        }
    }
}